import React from "react";
import { StyleSheet, Text, FlatList, Button, TextInput, View } from "react-native";
import Movie from "./src/shared/components/movie/movie";
import { movieService } from "./src/services/movie-service";
import Expo, { SQLite } from 'expo';

const db = SQLite.openDatabase('db.db');

export default class App extends React.Component {
  state = {
    movies: [],
    newMovieId: "",
    newMovieName: "",
    newMovieDate: ""
  };

  componentDidMount() {
    db.transaction(tx => {
      tx.executeSql(
        'create table if not exists movies (id integer primary key not null, name text, date text);'
      );
    });
    this.getMovies();
  }

  addMovie() {
    db.transaction(
      tx => {
        tx.executeSql('insert into movies (name, date) values (?, ?)', [this.state.newMovieName, this.state.newMovieDate]);
        tx.executeSql('select * from movies', [], (_, { rows }) =>
          console.log("Rows inserted", rows)
        );
      },
      null,
      this.update
    );
    var newMovie = {
      id: this.state.newMovieId,
      name: this.state.newMovieName,
      date: this.state.newMovieDate
    };

    movieService
      .addMovie(newMovie)
      .then(() => {
        this.getMovies();
      })
      .catch(error => {
        alert(error.message);
      });
  }

  getMovies() {
    movieService.getMovies().then(movies => {
      this.setState({ movies });
    });
  }

  getItemTemplate(movie) {
    return (<Movie movie={movie} />);
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <TextInput placeholder={"ID"} style={{height: 20, width: 40}} onChangeText={newMovieId => this.setState({ newMovieId })} />
          <TextInput placeholder={"NAME"}  onChangeText={newMovieName => this.setState({ newMovieName })} />
          <TextInput placeholder={"DATE"} onChangeText={newMovieDate => this.setState({ newMovieDate })} />
          <Button
            title={"Gravar"}
            onPress={() => this.addMovie()}
          />
        </View>
        <View>
          {JSON.stringify(this.state.movies)}
          <FlatList
            data={this.state.movies}
            renderItem={({ item }) => <Movie movie={item} />}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    backgroundColor: "#fff"
  }
});
