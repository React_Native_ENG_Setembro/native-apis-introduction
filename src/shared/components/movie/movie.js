import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Movie extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>{this.props.movie.id}</Text>
        <Text>{this.props.movie.name}</Text>
        <Text>{this.props.movie.date}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
});
