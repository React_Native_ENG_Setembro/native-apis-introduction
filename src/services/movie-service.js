import { AsyncStorage } from "react-native"

const movies_path_name = "movies";
export const movieService = {
    addMovie: (movie) => {
        return new Promise((resolve, reject) => {
            if(!movie.id){
                reject(new Error("Você esqueceu de preencher o ID"));
            }
            let movies = [];
            AsyncStorage.getItem(movies_path_name).then(value => {
                // Verifico se existem filmes gravados no banco
                // e transformo de volra para um array
                if(value){
                    movies = JSON.parse(value); // JSON.parse transforma uma string em um objeto javascript
                    movies = movies.filter(mv => mv.id != movie.id);
                }
                movies.push(movie);
                AsyncStorage.setItem(movies_path_name, JSON.stringify(movies)).then(() => {
                    resolve(true);
                });
            });
        });
    },

    getMovies: () => {
        return new Promise((resolve, reject) => {
            let movies = [];
            AsyncStorage.getItem(movies_path_name).then(value => {
                // Verifico se existem filmes gravados no banco
                // e transformo de volra para um array
                if(value){
                    movies = JSON.parse(value); // JSON.parse transforma uma string em um objeto javascript
                }
                resolve(movies);
            });
        });
    },

    removeMovie: (movieId) => {
        return new Promise((resolve, reject) => {
            let movies = [];
            AsyncStorage.getItem(movies_path_name).then(value => {
                // Verifico se existem filmes gravados no banco
                // e transformo de volra para um array
                if(value){
                    movies = JSON.parse(value); // JSON.parse transforma uma string em um objeto javascript
                    movies = movies.filter(mv => mv.id != movieId);
                    AsyncStorage.setItem(movies_path_name, JSON.stringify(movies)).then(() => {
                        resolve(true);
                    });
                }else{
                    resolve(true);
                }
            });
        });
    }
}